/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.GeneralLedger.service.Impl;

import in.ac.gpckasaragod.GeneralLedger.service.TransactionService;
import in.ac.gpckasaragod.GeneralLedger.ui.model.data.TransactionDetails;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class TransactionServiceImpl extends ConnectionServiceImpl implements TransactionService{  
    
    public String saveTransactionDetails(int id,int accountId,String transactionType,Date transactionDate,Double amount) throws SQLException {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO TRANSACTION DETAILS(ACCOUNT_ID,TRANSACTION_TYPE,TRANSACTION_DATE,AMOUNT) VALUES "
                    + "('"+id+"','"+accountId+"','"+transactionType+"','"+transactionDate+"','"+amount+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status != 1){
                return "save failed";
            }else {
                return "saved successfully";
                
            }
                
        } catch (SQLException ex) {
            Logger.getLogger(TransactionServiceImpl.class.getName()).log(Level.SEVERE, null,ex);
            return "save failed";
        }   
            
     } 
     public TransactionDetails readTransactionDetails(Integer id) {
        TransactionDetails transactionDetails = null;
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT *FROM TRANSACTION DETAILS";
            ResultSet resultSet =statement.executeQuery(query);
            while(resultSet.next()){
            Integer accountId = resultSet.getInt("ACCOUNT_ID");
            String transactionType=resultSet.getString("TRANSACTION_TYPE");
            Date transactionDate=resultSet.getDate("TRANSACTION_DATE");
            Double amount=resultSet.getDouble("AMOUNT");
            
        }
        return transactionDetails;    
     }catch(SQLException ex) {
         Logger.getLogger(TransactionServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
     }
        TransactionDetails TransactionDetails = null;
        
     return TransactionDetails;
  
     }
     public List<TransactionDetails> getAllTransactionDetailss(){
         List<TransactionDetails> transactionDetailss = new ArrayList<>();
         try{
             Connection connection = getConnection();
             Statement statement = connection.createStatement();
             String query = "SELECT * FROM TRANSACTIONDETAILS";
             ResultSet resultSet = statement.executeQuery(query);
             
             while(resultSet.next()){
                 Integer id = resultSet.getInt("ID");
                 Integer accountId = resultSet.getInt("ACCOUNT_ID");
                 String transactionType = resultSet.getString("TRANSACTION_TYPE");
                 Date transactionDate = resultSet.getDate("TRANSACTION_DATE");
                 Double amount = resultSet.getDouble("AMOUNT");
                 TransactionDetails transactionDetails = new TransactionDetails(id,accountId,transactionType,transactionDate,amount);
                 transactionDetailss.add(transactionDetails);
             }   
      } catch(SQLException ex)  {
          Logger.getLogger(TransactionServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
         
      }
         return transactionDetailss;
      }   
    public String updateTransactionDeatails(Integer id,Integer accountId,String transactionType,Date transactionDate,Double amount){
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE TRANSACTION DETAILS SET ACCOUNT_ID='"+accountId+"',TRANSACTION_TYPE='"+transactionType+"',TRANSACTIO_DATE='"+transactionDate+"',AMOUNT='"+amount+"'";
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
                return "Update failed";
            else
                return "Update successfully";
        }catch(SQLException ex){
            return "update failed";
        }
        
    }

    /**
     *
     * @param 
     * @return
     */
    public String deleteTransactionDetails(Integer id) {
         try {
             Connection connection = getConnection();
             String query = "DELETE FROM TRANSACTION DETAILS WHERE ID =?";
             PreparedStatement statement = connection.prepareStatement(query);
             statement.setInt(1, id);
             int delete = statement.executeUpdate();
             if(delete !=1)
                 return "Delete failed";
             else
                 return "Delete successfully";
         }catch (SQLException ex) {
             return "Delete failed";
         }
         
     }

    public String saveTransactionDetails(Integer accountId, String transactionType, Date transactionDate, Double amount) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO TRANSACTION DETAILS (ACCOUNT_ID,TRANSACTION_TYPE,TRANSACTION_DATE,AMOUNT) VALUES "
                    + "('"+accountId+"','"+transactionType+"','"+transactionDate+"','"+amount+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status != 1){
                return "save failed";
            }else {
                return "saved successfully";
                
            }
                
        } catch (SQLException ex) {
            Logger.getLogger(TransactionServiceImpl.class.getName()).log(Level.SEVERE, null,ex);
            return "save failed";
        }   
    }

    public TransactionDetails saveTransactionDetails(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String deleteTransactionDetails(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
             Connection connection = getConnection();
             String query = "DELETE FROM TRANSACTION_DETAILS WHERE ID =?";
             PreparedStatement statement = connection.prepareStatement(query);
             statement.setInt(1, id);
             int delete = statement.executeUpdate();
             if(delete !=1)
                 return "Delete failed";
             else
                 return "Delete successfully";
         }catch (SQLException ex) {
             return "Delete failed";
         }
    }

    @Override
    public String saveTransaction(Integer accountId, String transactionType, Date transactionDate, Double amount) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public TransactionDetails saveTransaction(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<TransactionDetails> getAllTransactions() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String updateTransaction(Integer id, Integer accountId, String transactionType, Date transactionDate, Double amount) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String deleteTransaction(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
         
         
        
            
        
        
    
         
            
            
            
            
            
        
    
        
    
    
    


    