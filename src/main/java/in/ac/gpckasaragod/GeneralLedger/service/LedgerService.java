/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.GeneralLedger.service;

import in.ac.gpckasaragod.GeneralLedger.ui.model.data.LedgerDetails;
import java.util.List;


/**
 *
 * @author student
 */
public interface LedgerService {
    public String saveLedgerDetails(String accountNo,String accountName);
    public LedgerDetails readLedgerDetails(Integer id);
    public List<LedgerDetails> getAllLedgers();
    public String updateLedgerDetails(Integer id,String accountNo,String accountName);
    public String deleteLedgerDetails(Integer id);
            
}
