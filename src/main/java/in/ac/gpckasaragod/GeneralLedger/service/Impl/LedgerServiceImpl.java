/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.GeneralLedger.service.Impl;

import in.ac.gpckasaragod.GeneralLedger.service.LedgerService;
import in.ac.gpckasaragod.GeneralLedger.ui.model.data.LedgerDetails;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class LedgerServiceImpl extends ConnectionServiceImpl implements LedgerService{
    
     public String saveLedgerDetails(int id, String accountNo,String accountName) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO LEDGER_DETAILS (ACCOUNT_NO,ACCOUNT_NAME) VALUES "
                    + "('"+id+"','"+accountNo+"','"+accountName+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status != 1){
                return "Save failed";
            }else {
                return "Saved successfully";
                
            }
        } catch (SQLException ex) {
             Logger.getLogger(LedgerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
             return "save failed";
          }
     }
  
    @Override
     public LedgerDetails readLedgerDetails(Integer id){
    LedgerDetails ledgerDetails = null;
    try{
         Connection connection = getConnection();
         Statement statement = connection.createStatement();
         String query = "SELECT *FROM LEDGER_DETAILS";
         ResultSet resultSet =statement.executeQuery(query);
         while(resultSet.next()){
             String accountNo = resultSet.getString("ACCOUNT_NO");
             String accountName=resultSet.getString("ACCOUNT_NAME");
             ledgerDetails=new LedgerDetails(id, accountNo,accountName);
         
         }
            
      }catch(SQLException ex) {  
             Logger.getLogger(LedgerServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
      }
      return ledgerDetails;
  }
    
     @Override
    public List<LedgerDetails> getAllLedgers() {
        List<LedgerDetails> ledgers = new ArrayList<>();
         try{
         Connection connection = getConnection();
         Statement statement = connection.createStatement();
         String query = "SELECT * FROM LEDGERDETAILS";
         ResultSet resultSet = statement.executeQuery(query);
         
         while(resultSet.next()){
             Integer id = resultSet.getInt("ID");
             String accountNo = resultSet.getString("ACCOUNT_NO");
             String accountName = resultSet.getString("ACCOUNT_NAME");
             LedgerDetails ledgerDetails = new LedgerDetails(id,accountNo,accountName);
             ledgers.add(ledgerDetails);
          }
     }  catch(SQLException ex) {
            Logger.getLogger(LedgerServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
     }
         return ledgers;
     }  
    public String updateLedgerDetails(Integer id, String accountNo, String accountName) {
         try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE LEDGER_DETAILS SET ACCOUNT_NO='"+accountNo+"',ACCOUNT_NAME='"+accountName+"'";
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
                return "Update failed";
            else
                return "Update successfully";
        }catch(SQLException ex){
            
             return "update failed";
        }
        
     }     
    
     @Override
    public String deleteLedgerDetails(Integer id) {
        try {
           Connection connection = getConnection();
           String query = "DELETE FROM LEDGER_DETAILS WHERE ID =?";
           PreparedStatement statement = connection.prepareStatement(query);
           statement.setInt(1, id);
           int delete = statement.executeUpdate();
           if(delete !=1)
               return "Delete failed";
           else
               return "Deleted successfully";
        } catch (SQLException ex) {
            return "Delete failed";
        }
    
   }

    @Override
    public String saveLedgerDetails(String accountNo, String accountName) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO LEDGER_DETAILS (ACCOUNT_NO,ACCOUNT_NAME) VALUES "
                    + "('"+accountNo+"','"+accountName+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status != 1){
                return "Save failed";
            }else {
                return "Saved successfully";
                
            }
        } catch (SQLException ex) {
             Logger.getLogger(LedgerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
             return "save failed";
          }
    }

}