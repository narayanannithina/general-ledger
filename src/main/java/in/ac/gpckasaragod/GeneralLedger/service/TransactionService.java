/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.GeneralLedger.service;

import in.ac.gpckasaragod.GeneralLedger.ui.model.data.TransactionDetails;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author student
 */
public interface TransactionService {
    public String saveTransaction(Integer accountId,String transactionType,Date transactionDate,Double amount);
    public TransactionDetails saveTransaction(Integer id);
    public List<TransactionDetails> getAllTransactions();
    public String updateTransaction(Integer id,Integer accountId,String transactionType,Date transactionDate,Double amount);
    public String deleteTransaction(Integer id);
    
}    
    
    
    
    

