/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.GeneralLedger.ui.model.data;

/**
 *
 * @author student
 */
public class LedgerDetails {
    private Integer id;
    private String accountNo;
    private String accountName;
   
    

    public LedgerDetails(Integer id, String accountNo, String accountName) {
        this.id = id;
        this.accountNo = accountNo;
        this.accountName = accountName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
    

}