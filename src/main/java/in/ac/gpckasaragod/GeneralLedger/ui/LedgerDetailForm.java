/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package in.ac.gpckasaragod.GeneralLedger.ui;

import in.ac.gpckasaragod.GeneralLedger.service.Impl.LedgerServiceImpl;
import in.ac.gpckasaragod.GeneralLedger.service.LedgerService;
import in.ac.gpckasaragod.GeneralLedger.ui.model.data.LedgerDetails;
import javax.swing.JOptionPane;

/**
 *
 * @author student
 */
public class LedgerDetailForm extends javax.swing.JPanel {
    private LedgerService ledgerService = new LedgerServiceImpl();
    private Integer selectedId;

    /**
     * Creates new form LedgerDetailForm
     */
    public LedgerDetailForm() {
        initComponents();
        
    }
    
    
       public LedgerDetailForm(LedgerDetails ledgerDetails) {
         initComponents();
         selectedId = ledgerDetails.getId();
         textAccountNo.setText(ledgerDetails.getAccountNo());
         textAccountName.setText(ledgerDetails.getAccountName());
         
         }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblLedgerForm = new javax.swing.JLabel();
        lblAccountNo = new javax.swing.JLabel();
        lblAccountName = new javax.swing.JLabel();
        textAccountNo = new javax.swing.JTextField();
        textAccountName = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();

        lblLedgerForm.setText("LEDGER_FORM");

        lblAccountNo.setText("ACCOUNT_NO");

        lblAccountName.setText("ACCOUNT_NAME");

        textAccountNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textAccountNoActionPerformed(evt);
            }
        });

        btnSave.setText("SAVE");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 313, Short.MAX_VALUE)
                .addComponent(lblLedgerForm)
                .addGap(248, 248, 248))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(236, 236, 236)
                        .addComponent(btnSave))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(105, 105, 105)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblAccountName)
                            .addComponent(lblAccountNo))
                        .addGap(197, 197, 197)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textAccountName, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textAccountNo, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addComponent(lblLedgerForm)
                .addGap(62, 62, 62)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAccountNo)
                    .addComponent(textAccountNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(58, 58, 58)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAccountName)
                    .addComponent(textAccountName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(84, 84, 84)
                .addComponent(btnSave)
                .addContainerGap(128, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(42, 42, 42))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        String accountNo = textAccountNo.getText();
        String accountName = textAccountName.getText();
         if(selectedId==null)
         {
             String status = ledgerService.saveLedgerDetails(accountNo,accountName);
             JOptionPane.showMessageDialog(LedgerDetailForm.this,status);
         }
         else{
             String status = ledgerService.updateLedgerDetails(selectedId,accountNo,accountName);
             JOptionPane.showMessageDialog(LedgerDetailForm.this,status);
         }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void textAccountNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textAccountNoActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_textAccountNoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSave;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblAccountName;
    private javax.swing.JLabel lblAccountNo;
    private javax.swing.JLabel lblLedgerForm;
    private javax.swing.JTextField textAccountName;
    private javax.swing.JTextField textAccountNo;
    // End of variables declaration//GEN-END:variables
}
